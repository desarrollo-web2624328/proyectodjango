from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views import View

@method_decorator(login_required, name='dispatch')
class HomeView(View):
    template_name = 'home.html'

    def get(self, request):
        # Agrega cualquier lógica necesaria aquí
        context = {
            # Agrega datos de contexto si es necesario
        }
        return render(request, self.template_name, context)