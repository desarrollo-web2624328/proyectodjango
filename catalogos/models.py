from django.db import models

# Create your models here.

class Pais(models.Model):
    nombre=models.CharField(max_length=20)

    def __str__(self):
        return self.nombre


class Departamento(models.Model):
    nombre=models.CharField(max_length=30)
    pais=models.ForeignKey(Pais,on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.nombre} ({self.pais.nombre})'


class Municipio(models.Model):
    nombre=models.CharField(max_length=30)
    departamento=models.ForeignKey(Departamento,on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.nombre} ({self.departamento.nombre})'
