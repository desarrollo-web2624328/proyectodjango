from rest_framework import generics
from django.shortcuts import render

from catalogos.models import Pais, Departamento, Municipio


# Create your views here.
class PaisVista(generics.ListCreateAPIView):
    queryset=Pais.objects.all()


class DepartamentoVista(generics.ListCreateAPIView):
    queryset=Departamento.objects.all()


class MunicipioVista(generics.ListCreateAPIView):
    queryset=Municipio.objects.all()





