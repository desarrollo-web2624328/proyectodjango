from django.contrib import admin

from catalogos.models import Pais, Departamento, Municipio


# Register your models here.
class PaisAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre')

admin.site.register(Pais, PaisAdmin)


class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','pais')

admin.site.register(Departamento, DepartamentoAdmin)


class MunicipioAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','departamento')

admin.site.register(Municipio, MunicipioAdmin)

