from django.contrib import admin
from django.urls import path, include

from home import views
from home.views import HomeView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('authPersonalizada.urls')),  # Incluye las URLs de la aplicación "authPersonalizada".
    path('',  HomeView.as_view(), name='home'),
]



