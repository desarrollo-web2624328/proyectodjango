from django.shortcuts import render

# Create your views here.

#autenticacion/views.py
from audioop import reverse

from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from rest_framework_simplejwt.tokens import Token
from .serializers import CustomUserSerializer
from .forms import CustomLoginForm, SignupForm  # Importa el formulario personalizado

class UserSignupView(View):
    template_name = 'authPersonalizada/signup.html'

    def get(self, request):
        form = SignupForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = SignupForm(request.POST)  # Obtén los datos enviados por el usuario

        if form.is_valid():
            serializer = CustomUserSerializer(data=form.cleaned_data)
            if serializer.is_valid():
                user = serializer.save()

                # Obtiene o crea un token para el usuario
                token, created = Token.objects.get_or_create(user=user)

                response_data = {
                    'user_id': user.id,
                    'username': user.username,
                    'access_token': token.key,
                }

                login_url = reverse_lazy('login')
                return redirect(login_url)

        return render(request, self.template_name, {'form': form})


class CustomLoginView(View):
    template_name = 'authPersonalizada/login.html'

    def get(self, request):
        form = CustomLoginForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = CustomLoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect(
                    'home')  # Cambia 'home' al nombre de la URL a la que deseas redirigir después del inicio de sesión
            else:
                form.add_error(None, 'Credenciales incorrectas')

        return render(request, self.template_name, {'form': form})
